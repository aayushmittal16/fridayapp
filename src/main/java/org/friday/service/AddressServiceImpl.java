package org.friday.service;

import org.friday.exception.InvalidAddressException;
import org.friday.model.Address;
import org.friday.util.Utilities;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    private static final String HOUSENUMBER_LAST_PATTERN = "((?:(?:\\p{L}+)(?:[ ]*))+)(?: )((?:[0-9a-zA-Z]+)(?:[ ]*)(?:[a-zA-Z)]?))";
    private static final String HOUSENUMBER_FIRST_PATTERN = "([a-zA-Z0-9]+)(?:[ ]+)((?:(?:\\p{L}+)(?:[ ]*))+)";
    private static final String TWO_NUMBERS_PATTERN = "((?:(?:\\p{L}+)(?:[ ]*))+(?:[0-9]+))(?: )((?:[0-9a-zA-Z]+)(?: )(?:[0-9]+))";

    /**
     * Delegates the task of parsing the given string address to parseAddress and returns the Address
     * @param address
     * @return Address object
     * @throws InvalidAddressException
     */
    @Override
    public Address getAddress(String address) throws InvalidAddressException {
        if(address == null || address.trim().equals("")){
            throw new InvalidAddressException();
        }
        address = address.trim().replaceAll(",", "");
        return parseAddress(address);
    }

    /**
     * Parses the input address string and returns an Address object type
     * @param address String
     * @return Address object
     * @throws InvalidAddressException
     */
    public Address parseAddress(String address) throws InvalidAddressException {
        String[] regexResults;
        if(Utilities.isRegexAMatch(address, TWO_NUMBERS_PATTERN)){
            regexResults = Utilities.matchAtCaptureGroup(address, TWO_NUMBERS_PATTERN);
            return new Address(regexResults[1], regexResults[0]);
        }
        else if(Utilities.isRegexAMatch(address, HOUSENUMBER_FIRST_PATTERN)){
            regexResults = Utilities.matchAtCaptureGroup(address, HOUSENUMBER_FIRST_PATTERN);
            return new Address(regexResults[0], regexResults[1]);
        }
        else if(Utilities.isRegexAMatch(address, HOUSENUMBER_LAST_PATTERN)){
            regexResults = Utilities.matchAtCaptureGroup(address, HOUSENUMBER_LAST_PATTERN);
            return new Address(regexResults[1], regexResults[0]);
        }
        else{
            throw new InvalidAddressException();
        }
    }

}
