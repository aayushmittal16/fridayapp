package org.friday.service;

import org.friday.exception.InvalidAddressException;
import org.friday.model.Address;

public interface AddressService {

    Address getAddress(String address) throws InvalidAddressException;
}
