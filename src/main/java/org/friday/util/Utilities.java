package org.friday.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

    /**
     * Checks if the given text has any matches when regex is applied
     * @param text Given address string
     * @param regex represents the pattern to be used
     * @return true or false
     */
    public static boolean isRegexAMatch(String text, String regex){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches() && matcher.group(0).equals(text);
    }

    /**
     * Returns street and housenumber
     * @param text Given address string
     * @param regex represents the pattern to be used
     * @return an array of string with two elements - housenumber and street and in no fixed order.
     * It depends on what regex string was passed
     */
    public static String[] matchAtCaptureGroup(String text, String regex){
        String[] matches = new String[2];
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        matcher.find();
        matches[0] = matcher.group(1);
        matches[1] = matcher.group(2);
        return matches;
    }
}
