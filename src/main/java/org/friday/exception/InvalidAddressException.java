package org.friday.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Invalid address format")
public class InvalidAddressException extends Exception {

    public static final String MESSAGE = "Invalid address format";

    public InvalidAddressException(){
        super(MESSAGE);
    }

}
