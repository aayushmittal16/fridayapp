package org.friday.controller;

import org.friday.exception.InvalidAddressException;
import org.friday.model.Address;
import org.friday.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @RequestMapping(method={RequestMethod.GET}, value = "/{address}")
    public ResponseEntity<Object> getAddress(
            @PathVariable(value="address") String address){
        Address addressObj = null;
        try {
            addressObj = addressService.getAddress(address);
        } catch (InvalidAddressException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(addressObj, HttpStatus.OK);
    }
}
