package org.friday.controller;

import org.friday.exception.InvalidAddressException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressController.class)
@AutoConfigureMockMvc
@ComponentScan("org.friday")
public class AddressControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AddressController addressController;

    @Test
    public void getAddress_thenReturnJson_AndStatus200() throws Exception {
        String[][] addressesAndResults = {
                //address string         housenumber    street
                {"Winterallee 3",          "3",      "Winterallee"},
                {"Musterstrasse 45",       "45",     "Musterstrasse"},
                {"Blaufeldweg 123B",       "123B",   "Blaufeldweg"},
                {"Am Bächle 23",           "23",     "Am Bächle"},
                {"Auf der Vogelwiese 23 b","23 b",   "Auf der Vogelwiese"},
                {"4, rue de la revolution","4",      "rue de la revolution"},
                {"200 Broadway Av",        "200",    "Broadway Av"},
                {"Calle Aduana, 29",       "29",     "Calle Aduana"},
                {"Calle 39 No 1540",       "No 1540","Calle 39"}
        };

        for (String[] addressesAndResult : addressesAndResults) {
            String uri = String.format("/address/%s",addressesAndResult[0]);

            mockMvc.perform(get(uri)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.houseNumber",is(addressesAndResult[1])))
                    .andExpect(jsonPath("$.street", is( addressesAndResult[2])));
        }

    }


    @Test
    public void getAddress_AndStatus404() throws Exception {
        String[] addresses = {"Talstrasse 30B House 50 59",
                              "Broadway 49 street",
                              " "
                                };

        for (String address : addresses) {
            String uri = String.format("/address/%s",address);

            mockMvc.perform(get(uri)
                    .contentType(MediaType.APPLICATION_JSON ))
                    .andExpect(status().isNotFound())
                    .andExpect(content().string(InvalidAddressException.MESSAGE));
        }

    }

}
