# FRIDAY App

An address provider returns addresses only with concatenated street names and numbers. Our own system on the other hand has separate fields for street name and street number.

Input: string of address

```sh
http://localhost:8080/address/21 Talstrasse
```


Output: string of street and string of house-number as JSON object


```sh
{
    houseNumber: "21",
    street: "Talstrasse"
}
```

## Running it

You can run it in two ways:

- The following command can be executed in the root directory of the project:
```sh
$   mvn spring-boot:run
```

- or you can use build a JAR file:
```sh
$   mvn package
```

- or you can use an already created JAR file in the ./jar folder:

```sh
$   java -jar ./jar/<jarname>.jar
```

## Usage

You have access to the following REST endpoint as given below:

```sh
/address/{addressString}
```

#### Examples:

```sh
http://localhost:8080/address/4 Broadway Street
http://localhost:8080/address/Calle 39 No 1540
```

You can open the above links in a browser or use a special program such as CURL. Please note that you might need to encode the URL before using it in programs such as CURL as they wouldn't read a URL with spaces in it.

## Swagger

You can look up the documentation of endpoints defined in this application by going to the following URL:

```sh
http://localhost:8080/swagger-ui.html
```